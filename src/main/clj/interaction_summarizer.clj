(ns interaction-summarizer
  (:require [clj-time.core :as time]
            [clj-time.format :as format]
            [clojure.java.io :as io]
            [clojure.zip :as zip]
            [clojure.data.zip.xml :as zx]
            [clojure.data.xml :as xml]
            [clojure-csv.core :as csv]))

(def input-map {:file-names [;; "ResultsOfInformalProcessDefinition1.xml"
                             "ResultsOfInformalProcessDefinition2.xml"
                             ;; "ResultsOfInformalProcessDefinition3.xml"
                             ;; "ResultsOfInformalProcessDefinition4.xml"
                             ;; "ResultsOfInformalProcessDefinition5.xml"
                             ;; "ResultsOfInformalProcessDefinition6.xml"
                             ;; "ResultsOfInformalProcessDefinition7.xml"
                             ]})

(defn- add-all-actions
  [m]
  {:pre [(:zipper m)]}
  (->> (zx/xml-> (:zipper m)
                 :RelevanceRelationship
                 :SourceEntities
                 :SourceInteractions
                 :Interaction
                 (zx/attr :action))
       vec
       (into #{})
       (assoc m :actions)))

(defn- add-all-relevant-resources
  [m]
  {:pre [(:zipper m)]}
  (->> (zx/xml-> (:zipper m)
                 :RelevanceRelationship
                 :RelevantCapabilityOrResource
                 :RelevantResource
                 zx/text)
       vec
       (assoc m :relevant-resources)))


(defn- add-all-relevant-capabilities
  [m]
  {:pre [(:zipper m)]}
  (->> (zx/xml-> (:zipper m)
                 :RelevanceRelationship
                 :RelevantCapabilityOrResource
                 :RelevantCapability
                 zx/text)
       vec
       (assoc m :relevant-capabilities)))

(defn- add-interactions-of-relevant-capabilities
  [m]
  (->> (zx/xml-> (:zipper m)
                 :RelevanceRelationship
                 (zx/seq-test [:RelevantCapabilityOrResource
                               :RelevantCapability
                               (zx/text= (:relevant-capability m))])
                 :SourceEntities
                 :SourceInteractions
                 :Interaction
                 (zx/attr :action))
       vec
       (merge [(:relevant-capability m)])
       (assoc m :interactions-of-relevant-capability)))

(defn- add-source-resources-of-relevant-capabilities
  [m]
  (->> (zx/xml-> (:zipper m)
                 :RelevanceRelationship
                 (zx/seq-test [:RelevantCapabilityOrResource
                               :RelevantCapability
                               (zx/text= (:relevant-capability m))])
                 :SourceEntities
                 :SourceInteractions
                 :Interaction
                 (zx/attr :action))
       vec
       (merge [(:relevant-capability m)])
       (assoc m :interactions-of-relevant-capability)))


(defn- add-interactions-of-relevant-resource
  [m]
  (->> (zx/xml-> (:zipper m)
                  :RelevanceRelationship
                  (zx/seq-test [:RelevantCapabilityOrResource
                                :RelevantResource
                                (zx/text= (:relevant-resource m))])
                  :SourceEntities
                  :SourceInteractions
                  :Interaction
                  (zx/attr :action))
       vec
       (merge [(:relevant-resource m)])
       (assoc m :interactions-of-relevant-resource)))




(defn- add-all-relevance-relationships-of-resource-correlation-coefficients
  [m]
  {:pre [(:zipper m)
         (:relevant-resource m)]}
  (->> (zx/xml-> (:zipper m)
                 :RelevanceRelationship
                 (zx/seq-test [:RelevantCapabilityOrResource
                               :RelevantResource
                               (zx/text= (:relevant-resource m))])
                 (zx/attr :correlationCoefficient))
       first
       (update-in m [:interactions-of-relevant-resource] conj)))

(defn- add-all-relevance-relationships-of-capability-correlation-coefficients
  [m]
  {:pre [(:zipper m)
         (:relevant-resource m)]}
  (->> (zx/xml-> (:zipper m)
                 :RelevanceRelationship
                 (zx/seq-test [:RelevantCapabilityOrResource
                               :RelevantCapability
                               (zx/text= (:relevant-capability m))])
                 (zx/attr :correlationCoefficient))
       first
       (update-in m [:interactions-of-relevant-capability] conj)))

(defn- add-all-relevance-relationships-of-resources
  [m]
  {:pre [(:zipper m)
         (:relevant-resources m)]}
  (->> m
       :relevant-resources
       (mapv #(-> (assoc m :relevant-resource %)
                  add-interactions-of-relevant-resource
                  add-all-relevance-relationships-of-resource-correlation-coefficients
                  :interactions-of-relevant-resource))
       (assoc m :interactions-of-relevant-resources)))

(defn- add-all-relevance-relationships-of-capabilities
  [m]
  {:pre [(:zipper m)
         (:relevant-capabilities m)]}
  (->> m
       :relevant-resources
       (mapv #(-> (assoc m :relevant-resource %)
                  add-interactions-of-relevant-resource
                  add-all-relevance-relationships-of-resource-correlation-coefficients
                  :interactions-of-relevant-capability))
       (assoc m :interactions-of-relevant-capabilities)))



(defn- add-zippers-for-parsed-xmls
  [m]
  (->> m
       :parsed-xml-map
       zip/xml-zip
       (assoc m :zipper)))

(defn- add-parsed-xml-map
  [m]
  (->> m
       :file-name
       io/resource
       io/input-stream
       xml/parse
       (assoc m :parsed-xml-map)))

(defn- add-headers-keywords-of-relevant-resources
  [m]
  (->> m
       :actions
       vec
       (mapv keyword)
       (into [:user-name])
       (assoc m :header-keywords)))

(defn- add-frequencies-of-headers
  [m]
  {:pre [(:header-keyword m)
         (:interactions-of-relevant-resource m)]}
  (->> m
       :interactions-of-relevant-resource
       second
       (filterv #(= (name (:header-keyword m)) %))
       count
       str
       (conj [(:header-keyword m)])
       (assoc m :header-frequency)))

(defn- add-corrrelation-coefficient
  [m]
  (assoc-in m [:row-content :correlation-coefficient] (last (:interactions-of-relevant-resource m))))

(defn- add-row-content
  [m]
  {:pre [(:header-keywords m)
         (:interactions-of-relevant-resource m)]}
  (->> m
       :header-keywords
       rest
       (mapv #(-> (assoc m :header-keyword %)
                  add-frequencies-of-headers
                  :header-frequency))
       (into [[(first (:header-keywords m)) (first (:interactions-of-relevant-resource m))]])
       (into {})
       (assoc m :row-content)
       add-corrrelation-coefficient))

(defn- add-row-contents-of-relevant-resources
  [m]
  {:pre [(:header-keywords m)
         (:interactions-of-relevant-resources m)]}
  (->> m
       :interactions-of-relevant-resources
       (mapv #(-> (assoc m :interactions-of-relevant-resource %)
                  add-row-content
                  :row-content))
       (assoc m :row-contents)))


(defn- add-process-data-to-final-row-set
  [m]
  {:pre [(:file-name m)]}
  (->> m
       :file-name
       (conj [])
       (conj [])
       (update-in m [:final-row-set] into)))

(defn- add-headers-to-final-row-set
  [m]
  {:pre [(:file-name m)]}
  (->> m
       :header-keywords
       (mapv name)
       vector
       (update-in m [:final-row-set] into)
       ))

(defn- add-rows-to-final-row-set
  [m]
  {:pre [(:file-name m)]}
  (->> m
       :row-contents
       (mapv #(->> m
                   :header-keywords
                   (mapv (fn [k] (k %)))))

       (update-in m [:final-row-set] into)))


(defn- add-headers-and-contents-of-relevant-resources
  [m]
  {:pre [(:row-contents m) (:header-keywords m) (:file-name m)]}
  (->> m
       add-rows-to-final-row-set
       add-headers-to-final-row-set
       add-process-data-to-final-row-set))

(defn- add-correlation-coefficient-keywords
  [m]
  (update-in m [:header-keywords] conj :correlation-coefficient))

(defn create-summary-of-relevance-relationships
  [m]
  {:pre [(:file-names m) (vector? (:file-names m))]}
  (->> m
       :file-names
       (mapv #(-> (assoc m :file-name %)
                  add-parsed-xml-map))
       (mapv add-zippers-for-parsed-xmls)
       (mapv add-all-actions)
       (mapv add-zippers-for-parsed-xmls)
       (mapv add-all-relevant-resources)
       (mapv add-zippers-for-parsed-xmls)
       (mapv add-all-relevance-relationships-of-resources)
       (mapv add-headers-keywords-of-relevant-resources)
       (mapv add-correlation-coefficient-keywords)
       (mapv add-row-contents-of-relevant-resources)
       (mapv add-headers-and-contents-of-relevant-resources)
       (mapv :final-row-set)
       (apply concat [])
       vec))



(->>  input-map
      create-summary-of-relevance-relationships
      csv/write-csv
      (spit "summary-file-new.csv"))
