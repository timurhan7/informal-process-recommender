package uni_stuttgart.ipsm.ipe_recommender;

public class Constants {


    public static final String CONFIG_FILE_NAME = "config.properties";
    public static final String CONFIG_VAR_IMPLEMENTATION_PACKAGES = "implementationPackages";


}
