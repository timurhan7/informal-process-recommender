package uni_stuttgart.ipsm.ipe_recommender.analyzers.crawlers;

import java.util.List;

import javax.xml.namespace.QName;

import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor;
import de.uni_stuttgart.iaas.ipsm.v0.TInteraction;
import uni_stuttgart.ipsm.ipe_recommender.utils.IInformalProcessDataStoreAccess;

public interface IGitHubInteractionCrawler {
	
	/**
	 *
	 *  @param toBeCrawledInstance  @return
	 * @param isAnalyzedResource Used to set the analyzed resource in the
 *            created interaction only interactions of existing elements in
 *            the modeling environment will be stored. Therefore the list of
 *            TNodeTemplates are searched
	 * @param xmlFileNameReader
	 * @param distance
	 */
	public List<TInteraction> crawlForInteractions(TInstanceDescriptor toBeCrawledInstance, boolean isAnalyzedResource, IInformalProcessDataStoreAccess xmlFileNameReader, int distance);
	
	/**
	 * Checks if the resource type is supported by this interaction crawler
	 *
	 * @param resourceDefinitionId
	 * @return
	 */
	boolean isSupportedResourceDefinitionId(QName resourceDefinitionId);
	
}
