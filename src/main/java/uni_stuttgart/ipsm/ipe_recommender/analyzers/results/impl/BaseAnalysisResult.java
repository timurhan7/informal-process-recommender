package uni_stuttgart.ipsm.ipe_recommender.analyzers.results.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;

//import org.oasis_open.docs.tosca.ns._2011._12.TTopologyTemplate;
import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor;
import de.uni_stuttgart.iaas.ipsm.v0.TInteraction;

import uni_stuttgart.ipsm.ipe_recommender.analyzers.results.IResourceAnalysisResult;

@Service
public class BaseAnalysisResult implements IResourceAnalysisResult {

	private TInstanceDescriptor targetResourceInstance;
	private QName targetResourceDefinitionId;
	//TTopologyTemplate neighborhoodTopology;
	List<TInteraction> interactions;
	private Map<TInstanceDescriptor, QName> inputResources;
	private int currentDistance;
	private Map<QName, List<TInteraction>> cachedInteractions = new HashMap<QName,List<TInteraction>>(); 





	public Map<QName, List<TInteraction>> getCachedInteractions() {
		return cachedInteractions;
	}

	public void setCachedInteractions(Map<QName, List<TInteraction>> cachedInteractions) {
		this.cachedInteractions = cachedInteractions;
	}

	@Override
	public void setInputResourceInstances(Map<TInstanceDescriptor, QName> inputResources) {
		this.inputResources = inputResources;
	}

	@Override
	public Map<TInstanceDescriptor, QName> getInputResources() {
		return this.inputResources;
	}

	@Override
	public void setDistance(int currentDistance) {
		this.currentDistance = currentDistance;
	}

	@Override
	public int getDistance() {
		return this.currentDistance;
	}


	//HumanRelevantResources relevantResources;
	Set<String> contentHashs;


	public BaseAnalysisResult() {
		super();
	}

	@Override
	public void setTargetResourceInstance(TInstanceDescriptor resourceInstance) {
		this.targetResourceInstance = resourceInstance;
	}

	@Override
	public TInstanceDescriptor getTargetResourceInstance() {
		return this.targetResourceInstance;
	}

	@Override
	public List<TInteraction> getInteractions() {
		if (this.interactions == null)
			this.interactions = new ArrayList<TInteraction>();
		return this.interactions;
	}

	@Override
	public void setInteractions(List<TInteraction> interactions) {
		this.interactions = interactions;
	}


	public Set<String> getContentHashs() {
		if (this.contentHashs == null)
			this.contentHashs = new HashSet<String>();
		return this.contentHashs;
	}

	public void setContentHashs(Set<String> contentHashs) {
		this.contentHashs = contentHashs;
	}

	@Override
	public boolean addInteraction(TInteraction interaction) {
            // check if interaction has been added previously
            if (!this.getContentHashs().contains(interaction.getDigest())) {
                this.getContentHashs().add(interaction.getDigest());
                this.getInteractions().add(interaction);
                return true;
            }
            return false;
	}



	@Override
	public QName getTargetResourceDefinitionId() {
		return this.targetResourceDefinitionId;
	}

	@Override
	public void setTargetResourceDefinitionId(QName targetResourceDefinitionId) {
		this.targetResourceDefinitionId = targetResourceDefinitionId;
	}

	@Override
	public void cacheResourceInteractions(QName analyzerTypeinstanceUri, List<TInteraction> interactions) {
		getCachedInteractions().put(analyzerTypeinstanceUri, interactions);
		
	}

	@Override
	public List<TInteraction> getCachedResourceInteractions(QName instanceUri) {
		if ( getCachedInteractions().get(instanceUri) != null ){
			System.out.println("CACHE HIT!!");
		} else{
			System.out.println("CACHE MISS");
		}
		
		return getCachedInteractions().get(instanceUri);
	}

	@Override
	public void reset() {
		this.interactions = new ArrayList<TInteraction>();
		
	}

}
