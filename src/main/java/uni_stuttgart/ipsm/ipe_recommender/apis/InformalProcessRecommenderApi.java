package uni_stuttgart.ipsm.ipe_recommender.apis;

import java.util.List;

import de.uni_stuttgart.iaas.ipsm.v0.TResourceDrivenProcessDefinition;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;

/**
 * Main functionality of this project is provided by this interface
 *
 * @author timur
 *
 */
public interface InformalProcessRecommenderApi {

	// TODO create an async version of this
	/**
	 *
	 * @param informalProcessDefinition, informalProcessInstances Get an
	 *            informal process instance analyze its resource instances using
	 *            interaction analyzers and return a proposed resource model
	 *            based on the analysis results.
	 * @return Recommendation model {@inheritDoc}
	 */
	TResourceDrivenProcessDefinition getRecommendationForInformalProcessInstances(TResourceDrivenProcessDefinition informalProcessDefinition, List<TResourceDrivenProcessDefinition> informalProcessInstances);

	TResourceDrivenProcessDefinition getRecommendationForInformalProcessInstancesUsingRelavanceRelationships(TResourceDrivenProcessDefinition informalProcessDefinition, List<TRelevanceRelationship> relevanceRelationships);

	List<TRelevanceRelationship> getRelevanceRelationshipsForInformalProcessInstances(TResourceDrivenProcessDefinition informalProcessDefinition, List<TResourceDrivenProcessDefinition> informalProcessInstances);
}
