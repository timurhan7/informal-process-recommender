package uni_stuttgart.ipsm.ipe_recommender.process_generator;

import java.util.List;

import de.uni_stuttgart.iaas.ipsm.v0.TResourceDrivenProcessDefinition;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;

public interface IInformalProcessDefinitionsGenerator {

	/**
	 * Generates a new informal process definition using the relevance
	 * relationships provided
	 *
	 * @param relevanceResults
	 * @param informalProcessDefinition
	 * @param inputResources
	 * @return
	 */
	TResourceDrivenProcessDefinition generateInformalProcessDefinition(TResourceDrivenProcessDefinition informalProcessDefinition, List<TRelevanceRelationship> relevanceResults);
}
