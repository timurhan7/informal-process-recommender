package uni_stuttgart.ipsm.ipe_recommender.process_generator.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.junit.internal.runners.model.EachTestNotifier;
import org.oasis_open.docs.tosca.ns._2011._12.TDefinitions;
import org.oasis_open.docs.tosca.ns._2011._12.TEntityTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TExtensibleElements;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TServiceTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TTopologyTemplate;
import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TResourceDrivenProcessDefinition;
import de.uni_stuttgart.iaas.ipsm.v0.TIntentionDefinition;
import de.uni_stuttgart.iaas.ipsm.v0.TOrganizationalCapabilityRefs;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevantCapability;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevantResource;
import de.uni_stuttgart.iaas.ipsm.v0.TResourceModel;
import uni_stuttgart.ipsm.ipe_recommender.process_generator.IInformalProcessDefinitionsGenerator;

/**
 * Simple treshhold based service
 *
 * @author sungurtn, song
 *
 */
@Service
public class BaseInformalProcessDefinitionsGenerator implements IInformalProcessDefinitionsGenerator {

	public static double FINAL_THRESH_HOLD = 0.9;


	private boolean nodeExists(QName type, List<TEntityTemplate> list) {
		/*two nodes are the same if their names are the same*/
		for (TEntityTemplate e : list) {
			if (e.getClass().equals(TNodeTemplate.class) && ((TNodeTemplate) e).getType().getLocalPart().equals(type.getLocalPart()) && ((TNodeTemplate) e).getType().getNamespaceURI().equals(type.getNamespaceURI())) {
				return true;
			}
		}
		return false;
	}


	@Override
	public TResourceDrivenProcessDefinition generateInformalProcessDefinition(TResourceDrivenProcessDefinition informalProcessDefinition, List<TRelevanceRelationship> relevanceResults) {

		TIntentionDefinition emptyIntentionDef = new TIntentionDefinition();

		emptyIntentionDef.setOrganizationalCapabilities(new TOrganizationalCapabilityRefs());
		/*initilization of input IP definition*/
		if (informalProcessDefinition.getResourceModel() == null)
			informalProcessDefinition.setResourceModel(new TResourceModel());

		if (informalProcessDefinition.getResourceModel().getDefinitions() == null)
			informalProcessDefinition.getResourceModel().setDefinitions(new TDefinitions());


		List<TExtensibleElements> tList = informalProcessDefinition.getResourceModel().getDefinitions().getServiceTemplateOrNodeTypeOrNodeTypeImplementation();
		/*add a service template in case there is none in the input*/
		if (tList.size() == 0)
			tList.add(new TServiceTemplate());

		/*add a topology template in case there is none in the input*/
		if (tList.get(0).getClass().equals(TServiceTemplate.class))
			if (((TServiceTemplate) tList.get(0)).getTopologyTemplate() == null)
				((TServiceTemplate) tList.get(0)).setTopologyTemplate(new TTopologyTemplate());

		/*the template list to add nodes to*/
		List<TEntityTemplate> tTemplates = ((TServiceTemplate) tList.get(0)).getTopologyTemplate().getNodeTemplateOrRelationshipTemplate();

		for (TRelevanceRelationship trr : relevanceResults) {
			/*without thresholds*/
			/*duplication check*/
			//for relevant resources
			if (trr.getRelevantCapabilityOrResource().getClass().equals(TRelevantResource.class)) {
				QName node = ((TRelevantResource) trr.getRelevantCapabilityOrResource()).getRelevantResource();
				TNodeTemplate recommendationResource = new TNodeTemplate();
				recommendationResource.setType(node);
				recommendationResource.setName("Recommended resource " + node.getLocalPart());
				if (!this.nodeExists(node, tTemplates) && trr.getCorrelationCoefficient() >= BaseInformalProcessDefinitionsGenerator.FINAL_THRESH_HOLD) {
					tTemplates.add(recommendationResource);
				}
			}

		}

		return informalProcessDefinition;
	}

}
