package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers;

import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;

public interface ICorrelationCoefficientCalculator {

    double calculateCorrelationCoefficient(double oldValue, double relevanceFactor, int distance);

    TRelevanceRelationship  updateCorrelationCoefficient(TRelevanceRelationship relevanceRelationship, double relevanceFactor, int distance);

}
