package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.impl;

import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.ICorrelationCoefficientCalculator;

@Service
public class CorrelationCoefficientCalculator implements ICorrelationCoefficientCalculator {


    /**
     *
     */
    public CorrelationCoefficientCalculator() {
    }

    private boolean controlRelevanceFactor(double relevanceFactor){

        if(-1 <= relevanceFactor && 1 >= relevanceFactor){
            return true;
        }
        else{
            return false;
        }

    }

    private boolean controlWeight(double weight){

        if(0 != weight){
            return true;
        }
        else{
            return false;
        }

    }

    private boolean controlOldValue(double oldValue){

        if(-1 <= oldValue && 1 >= oldValue){
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public double calculateCorrelationCoefficient(double oldValue, double relevanceFactor, int weight){
        if(controlWeight(weight))
        {
            double updateCoefficient = relevanceFactor / weight;
            double newValue = oldValue + updateCoefficient;

            return newValue;
        }
        else{
            return oldValue;
        }

    }

    @Override
    public TRelevanceRelationship updateCorrelationCoefficient(TRelevanceRelationship relevanceRelationship,double relevanceFactor, int distance) {
        double oldValue = relevanceRelationship.getCorrelationCoefficient();
        relevanceRelationship.setCorrelationCoefficient(calculateCorrelationCoefficient(oldValue, relevanceFactor, distance));
        return relevanceRelationship;
    }

}
