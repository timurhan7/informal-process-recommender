package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.impl;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TInteraction;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import de.uni_stuttgart.resource.InteractionMapping;
import de.uni_stuttgart.resource.InteractionMappings;
import de.uni_stuttgart.resource.TRelevanceMapping;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.impl.RelevanceMapperUtils;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.ICorrelationCoefficientCalculator;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.IRelevantCapabilityMapper;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.results.IRelevanceResults;
import uni_stuttgart.ipsm.ipe_recommender.utils.ICapabilityReader;
import uni_stuttgart.ipsm.ipe_recommender.utils.IInformalProcessDataStoreAccess;

@Service
public class GitHubInteractionBasedRelevantCapabilityMapper
    implements IRelevantCapabilityMapper{

    private ICapabilityReader capabilityReader;
    public final double BASE_CC_AND_RELEVANCE_FACTOR = 0.1;
    private ICorrelationCoefficientCalculator iCorrelationCoefficientCalculator;
    private IInformalProcessDataStoreAccess xmlFileNameReader;
    private InteractionMappings interactionMappings;

    @Autowired
    public GitHubInteractionBasedRelevantCapabilityMapper(ICapabilityReader capabilityReader, IInformalProcessDataStoreAccess xmlFileNameReader, ICorrelationCoefficientCalculator iCorrelationCoefficientCalculator) {
        setCapabilityReader(capabilityReader);
        setiCorrelationCoefficientCalculator(iCorrelationCoefficientCalculator);
        setXmlFileNameReader(xmlFileNameReader);
        setInteractionMappings(getXmlFileNameReader().getInteractionToRelationshipAndCapabilityMappings());
    }


    private List<TRelevanceMapping> findCapabilityMappingsForInteraction(String interactionType){
        List<TRelevanceMapping> mappings = new ArrayList<TRelevanceMapping>();

        for(InteractionMapping im : getInteractionMappings().getInteractionMapping()){
            if(im.getName().equalsIgnoreCase(interactionType)){
                for(TRelevanceMapping rm : im.getRelevanceMapping()){
                    if(rm!=null && rm.getRelevanceType().equals("relatedCapability"))
                        mappings.add(rm);
                }
            }
        }
        return mappings;
    }



	/**
         * This relevance mapper creates relevance relationships
         * things based on availabe interactions captured during the
         * previous process. Currently a file based approach is used
         * this should be extended the specific busines logic with
         * class specifications
	 * @param interpretationResult
	 * @return
	 *
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws JAXBException
	 */
    @Override
    public IRelevanceResults interpretResult(IRelevanceResults interpretationResult)  {

        /*Get the crawled list of relevance relationships*/
        List<TInteraction> interactions = interpretationResult.getResourceAnalysisResult().getInteractions();
        List<TRelevanceRelationship> relevanceRelationships = interpretationResult.getRelevanceRelationships();
        for(TInteraction currentInteraction : interactions){
            // find interaction capability types for the action represented by the interaction
            List<TRelevanceMapping> relevanceMappings = findCapabilityMappingsForInteraction(currentInteraction.getAction());
            int distance = currentInteraction.getDistance().intValue();
            for (TRelevanceMapping relevanceMapping : relevanceMappings){
                // Create the capability qname
                QName relevantCapabilityId = new QName(relevanceMapping.getTargetNamespace(), relevanceMapping.getName());
                TRelevanceRelationship relevanceRelationship = RelevanceMapperUtils.findRelevanceRelationshipForRelevantCapability(relevantCapabilityId, relevanceRelationships);
                // if relevance relationship does not exist create it
                if(relevanceRelationship == null){
                    relevanceRelationship = RelevanceMapperUtils.createRelevanceRelationshipForRelevantCapability(relevantCapabilityId, BASE_CC_AND_RELEVANCE_FACTOR);
                    relevanceRelationships.add(relevanceRelationship);
                }
                // Interaction has been already checked
                if(RelevanceMapperUtils.isInteractionAlreadyIncluded(relevanceRelationship, currentInteraction)){
                    continue;
                }

                getiCorrelationCoefficientCalculator().updateCorrelationCoefficient(relevanceRelationship, relevanceMapping.getRelevanceFactor(), distance);
                RelevanceMapperUtils.addInteractionIntoRelevanceRelationshipSources(relevanceRelationship, currentInteraction);
            }
        }
        return interpretationResult;
    }

    /**
     * @return the capabilityReader
     */
    public ICapabilityReader getCapabilityReader() {
        return capabilityReader;
    }

    /**
     * @param capabilityReader the capabilityReader to set
     */
    public void setCapabilityReader(ICapabilityReader capabilityReader) {
        this.capabilityReader = capabilityReader;
    }

    /**
     * @return the iCorrelationCoefficientCalculator
     */
    public ICorrelationCoefficientCalculator getiCorrelationCoefficientCalculator() {
        return iCorrelationCoefficientCalculator;
    }

    /**
     * @param iCorrelationCoefficientCalculator the iCorrelationCoefficientCalculator to set
     */
    public void setiCorrelationCoefficientCalculator(
        ICorrelationCoefficientCalculator iCorrelationCoefficientCalculator) {
        this.iCorrelationCoefficientCalculator = iCorrelationCoefficientCalculator;
    }

    /**
     * @return the xmlFileNameReader
     */
    public IInformalProcessDataStoreAccess getXmlFileNameReader() {
        return xmlFileNameReader;
    }

    /**
     * @param xmlFileNameReader the xmlFileNameReader to set
     */
    public void setXmlFileNameReader(IInformalProcessDataStoreAccess xmlFileNameReader) {
        this.xmlFileNameReader = xmlFileNameReader;
    }

    /**
     * @return the interactionMappings
     */
    public InteractionMappings getInteractionMappings() {
        return interactionMappings;
    }

    /**
     * @param interactionMappings the interactionMappings to set
     */
    public void setInteractionMappings(InteractionMappings interactionMappings) {
        this.interactionMappings = interactionMappings;
    }

}
