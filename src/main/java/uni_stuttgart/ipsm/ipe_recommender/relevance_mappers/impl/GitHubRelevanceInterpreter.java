package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.impl;


import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;

import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TInteraction;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import de.uni_stuttgart.resource.InteractionMapping;
import de.uni_stuttgart.resource.TAvailableResourceModelElements;
import de.uni_stuttgart.resource.TRelevanceMapping;

import uni_stuttgart.ipsm.ipe_recommender.analyzers.impl.GitConstants;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.impl.RelevanceMapperUtils;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.ICorrelationCoefficientCalculator;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.IRelevantResourceMapper;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.results.IRelevanceResults;
import uni_stuttgart.ipsm.ipe_recommender.utils.IInformalProcessDataStoreAccess;

@Service
public class GitHubRelevanceInterpreter implements IRelevantResourceMapper{


    private List<TNodeTemplate> additionalRepos;
    private int depth;
    private IInformalProcessDataStoreAccess iXmlFileNameReader;
    private ICorrelationCoefficientCalculator iCorrelationCoefficientCalculator;
    public final double BASE_CORRELATION_COEFFICIENT = 0.1;

    @Autowired
    public GitHubRelevanceInterpreter(IInformalProcessDataStoreAccess iXmlFileNameReader, ICorrelationCoefficientCalculator iCorrelationCoefficientCalculator){
        setiCorrelationCoefficientCalculator(iCorrelationCoefficientCalculator);
        setiXmlFileNameReader(iXmlFileNameReader);
        TAvailableResourceModelElements additionalReposXml = getiXmlFileNameReader().getAdditionalGitHubRepositoriesToBeCrawled();
        this.additionalRepos = new ArrayList<TNodeTemplate>();
        this.additionalRepos.addAll(additionalReposXml.getNodeTemplate());

    }

    public List<TNodeTemplate> getAvailableResourceModelElements(){
        return this.additionalRepos;
    }



    private TRelevanceMapping findResourceMappingForInteraction(String interactionType){
        for(InteractionMapping im : getiXmlFileNameReader().getInteractionToRelationshipAndCapabilityMappings().getInteractionMapping()){
            if(im.getName().equalsIgnoreCase(interactionType)){
                for(TRelevanceMapping rm : im.getRelevanceMapping()){
                    if(rm != null && rm.getRelevanceType().equals("relatedResources"))
                        return rm;
                }
            }
        }
        return null;
    }


    private boolean isInteractionSupported(TInteraction interaction){
        return interaction.getEntityIdentity().getName().equals(GitConstants.GIT_INTERACTION_NAME) && !interaction.getEntityIdentity().getName().equals(GitConstants.GIT_INTERACTION_NS);
    }

    @Override
    public IRelevanceResults interpretResult(IRelevanceResults interpretationResult){
        // get crawled interaction results
        List<TInteraction> interactions = interpretationResult.getResourceAnalysisResult().getInteractions();
        // get relevance relationship list
        List<TRelevanceRelationship> relevanceRelationships = interpretationResult.getRelevanceRelationships();

        for(TInteraction interaction : interactions){
            // check if interaction is a Git interaction
            if(!isInteractionSupported(interaction)) {
                continue;
            }

            // Get depth value of this analysis
            this.depth = interaction.getDistance().intValue();
            /*initialization of indexes*/
            // set up github NodeTemplate instance
            String identifiedResource = interaction.getIdentifiedResource().getInstanceUri();
            QName identifiedResourceDefinitionId = getiXmlFileNameReader().getResourceDefinitionBasedOnInstanceUri(identifiedResource);
            // no need to continue as relevant resource could not be identified for this id
            if(identifiedResourceDefinitionId == null){
                System.err.println("Instance type could not be identified " + identifiedResource);
                continue;
            }

            // find corresponding relationshipType and weight for this interaction
            TRelevanceMapping relevanceMapping = findResourceMappingForInteraction(interaction.getAction());
            if(relevanceMapping==null){
                System.err.println("Please specify InteractionMapping(interaction->relationship) for interaction type "
                                   +interaction.getAction()+", this interaction type will not be "
                                   + "interpreted in this execution.");
                continue;
            }
            TRelevanceRelationship relevanceRelationship =  RelevanceMapperUtils.findRelevanceRelationshipForRelevantResource(identifiedResourceDefinitionId, relevanceRelationships);
            // No relevance relationship exists add new one
            if(relevanceRelationship == null){
                relevanceRelationship = RelevanceMapperUtils.createRelevanceRelationshipForRelevantResource(identifiedResourceDefinitionId, BASE_CORRELATION_COEFFICIENT);
                // update list of relevance relationships
                relevanceRelationships.add(relevanceRelationship);
            }

            // Check if the interaction is already used for
            // calculating the relevance relationship if yes continue
            // without adding it again
            if(RelevanceMapperUtils.isInteractionAlreadyIncluded(relevanceRelationship, interaction)){
                continue;
            }
            // Finally update the correlation coefficient and add the interaction
            getiCorrelationCoefficientCalculator().updateCorrelationCoefficient(relevanceRelationship, relevanceMapping.getRelevanceFactor(), this.depth);
            RelevanceMapperUtils.addInteractionIntoRelevanceRelationshipSources(relevanceRelationship, interaction);
        }

        return interpretationResult;
    }

    /**
     * @return the iXmlFileNameReader
     */
    public IInformalProcessDataStoreAccess getiXmlFileNameReader() {
        return iXmlFileNameReader;
    }

    /**
     * @param iXmlFileNameReader the iXmlFileNameReader to set
     */
    public void setiXmlFileNameReader(IInformalProcessDataStoreAccess iXmlFileNameReader) {
        this.iXmlFileNameReader = iXmlFileNameReader;
    }

    /**
     * @return the iCorrelationCoefficientCalculator
     */
    public ICorrelationCoefficientCalculator getiCorrelationCoefficientCalculator() {
        return iCorrelationCoefficientCalculator;
    }

    /**
     * @param iCorrelationCoefficientCalculator the iCorrelationCoefficientCalculator to set
     */
    public void setiCorrelationCoefficientCalculator(
        ICorrelationCoefficientCalculator iCorrelationCoefficientCalculator) {
        this.iCorrelationCoefficientCalculator = iCorrelationCoefficientCalculator;
    }

}
