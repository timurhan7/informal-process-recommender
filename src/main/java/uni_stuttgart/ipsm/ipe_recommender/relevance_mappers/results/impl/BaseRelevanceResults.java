package uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.results.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import uni_stuttgart.ipsm.ipe_recommender.analyzers.results.IResourceAnalysisResult;
import uni_stuttgart.ipsm.ipe_recommender.relevance_mappers.results.IRelevanceResults;

@Service
public class BaseRelevanceResults implements IRelevanceResults{

	private IResourceAnalysisResult resourceAnalysisResult;
	private List<TRelevanceRelationship> relevanceRelationships;
	int depth=1;//initial value of depth
	private List<TRelevanceRelationship> oldRelevanceRelationships;
	
	@Override
	public IResourceAnalysisResult getResourceAnalysisResult() {
		if(resourceAnalysisResult == null){
			System.err.println("No valid IResourceAnalysisResult passed to interpreter. Going to terminate.");
			System.exit(0);
		}
		return resourceAnalysisResult;
	}

	@Override
	public void setResourceAnalysisResult(IResourceAnalysisResult resourceAnalysisResult) {
		this.resourceAnalysisResult = resourceAnalysisResult;
	}

	@Override
	public List<TRelevanceRelationship> getRelevanceRelationships() {
		if(relevanceRelationships == null)
			relevanceRelationships = new ArrayList<TRelevanceRelationship>();
		return relevanceRelationships;
	}


	@Override
	public int getDistance() {
		return this.depth;
	}


	@Override
	public void setDistance(int depth) {
		this.depth = depth;
	}

	@Override
	public List<TRelevanceRelationship> getOldRelevanceRelationships() {
		if(this.oldRelevanceRelationships==null)
			this.oldRelevanceRelationships = new ArrayList<TRelevanceRelationship>();
		return this.oldRelevanceRelationships;
	}

	@Override
	public void reset() {
		this.relevanceRelationships = new ArrayList<TRelevanceRelationship>();
		this.depth = 0;
		
	}
	

}
