package uni_stuttgart.ipsm.ipe_recommender.utils;

import java.util.Collection;

import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor;
import de.uni_stuttgart.iaas.ipsm.v0.TInteractiveInitializableEntityDefinition;

import uni_stuttgart.iaas.ipsm.utils.SchemaAccess;

public class EntityAccessUtils {

	public static String getNameOfInteractiveInitableEntity(TInteractiveInitializableEntityDefinition iied){
		return iied.getInitializableEntityDefinition().getIdentifiableEntityDefinition().getEntityIdentity().getName();
		
	}

	public static String getTargetNamespaceOfInteractiveInitableEntity(
			TInteractiveInitializableEntityDefinition iied) {
		return iied.getInitializableEntityDefinition().getIdentifiableEntityDefinition().getEntityIdentity().getTargetNamespace();
		
	}
	
	/**
	 * This method is to be used when adding an input resource to the analysis
	 * list. Since we don't want interaction duplicates, there should be no
	 * duplicates of nodes.
	 *
	 * @param node
	 * @param nodes {@inheritDoc}
	 */
	public static boolean isInstanceAlreadyInCollection(TInstanceDescriptor instance, Collection<TInstanceDescriptor> instanceMap) {
		// add a node to the node list only if its name and instance descriptor location
		// is not identical to any in the list*/
		for (TInstanceDescriptor tempInstance : instanceMap) {
			/*first check node name and type*/
			if (SchemaAccess.doInstanceDescriptorsRepresentSameExternalEntity(instance, tempInstance)) {
				return true;
			}
		}

		return false;

	}

}
