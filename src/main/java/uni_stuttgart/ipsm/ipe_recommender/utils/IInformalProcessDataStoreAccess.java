package uni_stuttgart.ipsm.ipe_recommender.utils;


import java.util.List;

import javax.xml.namespace.QName;

import de.uni_stuttgart.iaas.ipsm.v0.TInstanceDescriptor;
import de.uni_stuttgart.resource.InteractionMappings;
import de.uni_stuttgart.resource.TAvailableCapabilityDefinitions;
import de.uni_stuttgart.resource.TAvailableResourceDefinitions;
import de.uni_stuttgart.resource.TAvailableResourceModelElements;
import de.uni_stuttgart.resource.TOrganizationalDefinitions;

public interface IInformalProcessDataStoreAccess {

    public TAvailableResourceModelElements getAdditionalGitHubRepositoriesToBeCrawled();

    public TAvailableResourceDefinitions getAvailableResourceDefinitions();

    public TAvailableCapabilityDefinitions getAvailableCapabilityDefinitions();

    public InteractionMappings getInteractionToRelationshipAndCapabilityMappings() ;

    public TOrganizationalDefinitions getOrganizationalDefinitions();

    public TInstanceDescriptor getInstanceDescriptorBasedOnInstanceUri(String uri);

    public QName getResourceDefinitionBasedOnInstanceUri(String uri);

    public List<TInstanceDescriptor> getInstanceDescriptorsOfAResourceDomain(String uri);

    public List<TInstanceDescriptor> getInstanceDescriptorsOfAResourceDefintion(QName resourceDefinitionId);
    
    public List<TInstanceDescriptor> getInstanceDescriptorsWithInstanceURIsStratingWith(String startPath);

}
