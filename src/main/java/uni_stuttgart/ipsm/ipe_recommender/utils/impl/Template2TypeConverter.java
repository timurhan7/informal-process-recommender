package uni_stuttgart.ipsm.ipe_recommender.utils.impl;

import org.oasis_open.docs.tosca.ns._2011._12.TNodeTemplate;
import org.oasis_open.docs.tosca.ns._2011._12.TNodeType;

import uni_stuttgart.ipsm.ipe_recommender.utils.ITemplate2TypeConverter;

public class Template2TypeConverter implements ITemplate2TypeConverter{

	@Override
	public TNodeType tNodeTemplate2tNodeType(TNodeTemplate nodeTemplate) {
		TNodeType nodeType = new TNodeType();
		nodeType.setName(nodeTemplate.getType().getLocalPart());
		nodeType.setTargetNamespace("http://www.uni-stuttgart.de/resource");
		
		return nodeType;
	}

}
