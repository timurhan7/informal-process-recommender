package uni_stuttgart.ipsm.ipe_recommender;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;

import de.uni_stuttgart.iaas.ipsm.v0.ObjectFactory;
import de.uni_stuttgart.iaas.ipsm.v0.TResourceDrivenProcessDefinition;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationship;
import de.uni_stuttgart.iaas.ipsm.v0.TRelevanceRelationships;
import uni_stuttgart.ipsm.ipe_recommender.apis.InformalProcessRecommenderApi;

public class TTest {

	String[] listOfInputFileNames = {"InformalProcessDefinition1", "InformalProcessDefinition2", "InformalProcessDefinition3", "InformalProcessDefinition4", "InformalProcessDefinition5", "InformalProcessDefinition6", "InformalProcessDefinition7"};
	// String[] listOfInputFileNames = {"InformalProcessDefinition3"};


	@Test
	public void generateRelevanceRelationships() {
		/*Initialize a recommender object*/
		InformalProcessRecommenderApi iprs = new InformalProcessRecommenderSpringBased();
		try {

			for (String fileName : this.listOfInputFileNames) {
				URL pathToSourceFile = this.getClass().getClassLoader().getResource(fileName + ".xml");
				File parentFolder = (new File(pathToSourceFile.getPath())).getParentFile();
				InputStream fileInputStream = pathToSourceFile.openStream();
				JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
				Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
				/*Get informal process instances*/
				JAXBElement<TResourceDrivenProcessDefinition> processInstanceJaxb = (JAXBElement<TResourceDrivenProcessDefinition>) unmarshaller.unmarshal(fileInputStream);
				System.out.println(fileName + " will be analyzed...");
				TResourceDrivenProcessDefinition informalProcessDefinition = processInstanceJaxb.getValue();

				List<TRelevanceRelationship> relevanceRelationships = iprs.getRelevanceRelationshipsForInformalProcessInstances(informalProcessDefinition, new ArrayList<TResourceDrivenProcessDefinition>());

				TRelevanceRelationships relevanceRelationshipsJaxb = new TRelevanceRelationships();
				relevanceRelationshipsJaxb.getRelevanceRelationship().addAll(relevanceRelationships);
				ObjectFactory objFactory = new ObjectFactory();
				Marshaller marshaller = jaxbContext.createMarshaller();
				marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
				marshaller.marshal(objFactory.createRelevanceRelationships(relevanceRelationshipsJaxb), new File(parentFolder, "ResultsOf" + fileName + ".xml"));

				TResourceDrivenProcessDefinition recommendationModel = iprs.getRecommendationForInformalProcessInstancesUsingRelavanceRelationships(informalProcessDefinition, relevanceRelationships);
				marshaller.marshal(objFactory.createResourceDrivenProcessDefinition(recommendationModel), new File(parentFolder, "RecommendationModelOf" + fileName + ".xml"));

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
